//======================================================================================
/** \file DCMotor.cpp
 *    This file contains a DCMotor class. It includes methods that child
 *    classes will need to implement in order to comply with the standard coding
 *    practices used in the ME 507 Atmospheric Dispersion Corrector project.
 *
 *  Revisions:
 *    \li 12-05-2013 GDG Original file taken from StepperMotor
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************


#include "emstream.h"                       // Header for serial ports and devices
#include "FreeRTOS.h"                       // Header for the FreeRTOS RTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // Header for FreeRTOS queues
#include "semphr.h"                         // Header for FreeRTOS semaphores
#include "DCMotor.h"


//-------------------------------------------------------------------------------------
/** \brief This class should run the DCMotor on an AVR processor.
 *  \details This initializes functions to be used in the DCMotor class. This constructor
 *  also calls the parent constructor from \c Motor.
 *  @param newName Name to be passed to \c Motor parent constructor
 *  @param motor_num Indicates which motor is being controlled
 *  @param p_serial Pointer to a serial device (port, radio, SD card, etc.) which can
 *                   be used by this task to communicate (default: NULL)
 */
DCMotor::DCMotor (const char* newName, uint8_t motor_num, emstream* p_serial) :
    Motor(newName, p_serial)
{
    motor_number = motor_num;

    switch(motor_number)
    {
        case(1):
            p_motor = new DRV8833_driver (0, 1, p_serial);
            p_encoder = new encoder_driver (1, p_serial);
            p_PID_position = new PID_controller (500, 0.0, 6.0 , p_serial, 3000);
			p_calSensor = new IRSensor ("IR1", 1, p_serial);
            break;
        case(2):
            p_motor = new DRV8833_driver (0, 2, p_serial);
            p_encoder = new encoder_driver (2, p_serial);
            p_PID_position = new PID_controller (300, 50.0, 5.0 , p_serial, 3000);
            p_calSensor = new IRSensor ("IR2", 2, p_serial);
            break;
    }
}

/** \brief This function turns on the motor.
 * \details This function sets the necessary drivers to on.
 */
void DCMotor::on (void)
{
    p_motor->enable();
}

/** \brief This function turns off the motor.
 * \details This function sets the necessary drivers to off.
 */
void DCMotor::off (void)
{
    p_motor->set_power(0);
    p_motor->disable();
}

/** \brief This function toggles the brake of the motor.
 * \details This function determines if the brake is on or off and toggles the value.
 * This is done by enabling or disabling the coils.
 */
void DCMotor::brake (void)
{
    // Turns out we didn't need it... so didn't code it.
}

/** \brief Moves the DC motor to a position specified
 * \details This function sets the position the motor should move to and then sets power 
 * to move there.
 * @param pos_des Position to move to.
 */
void DCMotor::goToPos (int32_t pos_des)
{
    // Get current position to be used in control loop
    int32_t position = p_encoder->get_position();

    // Send desired (shared value) and current position to PID for calculation
    float intermediate_calc = p_PID_position->UpdatePID(pos_des,position);
    int16_t motor_input;

    // Saturate values to a uint16 and divide by 100 (uniform gain change for simplicity)
    if(intermediate_calc > 32767)
        motor_input = 32767/100;
    else if(intermediate_calc < -32768)
        motor_input = -32768/100;
    else
        motor_input = (int16_t)(intermediate_calc/100);

    // Saturate motor inputs to usable range [-100:100]
    if(motor_input>100)
        motor_input=100;
    else if(motor_input<-100)
        motor_input=-100;

    // Send values to motor 2
    p_motor->set_power(motor_input);
}

/** \brief Moves the DC motor to a velocity specified
 * \details This function sets the velocity the motor should move and then sets power 
 * to move there.  Needs to be called at a regular interval.
 * @param vel Velocity to move to.
 */
void DCMotor::goToVel (int16_t vel)
{
    // Didn't need it, didn't code it
}

/** \brief This function returns the position from the encoder.
 * \details This code reads the position from the encoder and returns a signed 32-bit value.
 * @return Position of motor.
 */
int32_t DCMotor::getPos (void)
{
    return p_encoder->get_position();
}

/** \brief This function sets the position of the encoder.
 * \details This code sets the encoder value to the input value specified.
 * @param pos Position to set the encoder to.
 */
void DCMotor::setPos (int32_t pos)
{
    p_encoder->set_position(pos);
}

/// This function sets the velocity to move at.
void DCMotor::setVel (int16_t)
{
    // Didn't need it, didn't code it
}

/** \brief This function calibrates the motor with the IR sensor.
 * \details This code turns on the IR sensor and sets power to move until the IR sensor is
 * triggered. 
 * @return Motor calibration state.
 */
bool DCMotor::calibrate (void)
{
	static bool first_run = true;
	if (first_run)
	{
		p_calSensor->startCalibration();
        first_run = 0;
    }
    
    bool IR = p_calSensor->isTriggered();
    switch(IR)
    {
        case 0:
            p_motor->set_power(40);
            return 0;
            break;
            
        case 1:
            setPos(0);
            p_motor->set_power(0);
            p_calSensor->endCalibration();
            first_run = 1;
            return 1;
            break;

        default:
            return 0;
            break;
    }
}

/** \brief This function sets the power output to the motor.
 * \details This code sets the power for the motor to operate at.
 * @param power Power to set motor to.
 */
void DCMotor::set_power (int16_t power)
{
    p_motor->set_power(power);
}

/// This function intializes the position PID control for the motor.
void DCMotor::initPosPID (void)
{
    p_PID_position->InitializePID((float)(p_encoder->get_position()));
}

/// This function intializes the velocity PID control for the motor.
void DCMotor::initVelPID (void)
{
    // Didn't need it, didn't code it
}

/// This function overloads the << operator for printing messages.
emstream& operator<< (emstream& out, const DCMotor& motor)
{
    out << motor.name << endl;
    return out;
}
