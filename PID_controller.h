//*************************************************************************************
/** \file PID_controller.h
 *    This file contains functions for a customizable PID controller.
 *    Information involving programming a PID controller in C received from the article "PID
 *    without a PhD" at 
 *    <http://www.embedded.com/design/prototyping-and-development/4211211/PID-without-a-PhD>
 *
 *  Revisions:
 *    \li 03-07-2013 GDG Original file
 *    \li 03-14-2013 GDG Code updated while testing class
 *    \li 03-15-2013 GDG Code finalized
 *
 *  License:
 *    This file is copyright 2013 by GD Gudgel and released under the Lesser GNU 
 *    Public License, version 2. It intended for educational use only, but its use
 *    is not limited thereto. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

// This define prevents this .H file from being included multiple times in a .CPP file
#ifndef _AVR_PIDcontroller_H_
#define _AVR_PIDcontroller_H_

#include "emstream.h"							// Header for serial ports and devices
#include "FreeRTOS.h"							// Header for the FreeRTOS RTOS
#include "task.h"								// Header for FreeRTOS task functions
#include "queue.h"								// Header for FreeRTOS queues
#include "semphr.h"								// Header for FreeRTOS semaphores

#include "time_stamp.h"							// Class to implement a microsecond timer


//-------------------------------------------------------------------------------------
/** \brief This class provides a PID controller for use with RC motors
 *  \details This initializes functions to be used in the PID_controller class. Function InitializePID()
 *  must be called at the beginning of any control sequence because it sets values for I_sum and time_old 
 *  to be used in subsequent control loops.  UpdatePID() will be called from that point forward to update
 *  the control value outputed from the calculations within the function, and also updates the I_sum and
 *  time_old value for subsequent control loops.
 */

class PID_controller
{
	protected:
		/// The PID_controller class uses this pointer to the serial port to say hello
		emstream* ptr_to_serial;
		
		/// Porportional (K_p) gain
		float P_gain;
		/// Integral (K_i) gain
		float I_gain;
		/// Derivative (K_d) gain
		float D_gain;
		
		/// Current error sum for integrator
		float I_sum;
		
		/// Maximum sum integrator can achieve (magnitude)
		float I_sum_max;
		
		/// Position of last run through code
		int16_t position_old;
		
		/// Keeper of the time...
		time_stamp current_time;
		
		/// Time in microseconds of last call of UpdatePID
		uint32_t time_old;

    public:
		// The constructor sets up a PID controller for use.
		PID_controller (float, float, float, emstream*, float);

		// This function reads clears the integrator error sum as well as records the
		// current time and position for use in next UpdatePID call
		void InitializePID (float);

		// This function updates the PID control value by calculating P, I, and D term
		// values from the error (difference in desired vs. actual position) and the 
		// change in time since last call (time_new - time_old)
		float UpdatePID (float, float);
		
		// This function gives information pertaining to the PID controller including
		// current gains, current and desired position, and integrator sum.
		void about(void);

}; // end of class PID_controller

#endif // _AVR_PIDcontroller_H_
