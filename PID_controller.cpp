//*************************************************************************************
/** \file PID_controller.cpp
 *    This file contains functions for a customizable PID controller.
 *    Information involving programming a PID controller in C received from the article "PID
 *    without a PhD" at 
 *    <http://www.embedded.com/design/prototyping-and-development/4211211/PID-without-a-PhD>
 *
 *  Revisions:
 *    \li 03-07-2013 GDG Original file
 *    \li 03-14-2013 GDG Code updated while testing class
 *    \li 03-15-2013 GDG Code finalized
 *
 *  License:
 *    This file is copyright 2013 by GD Gudgel and released under the Lesser GNU 
 *    Public License, version 2. It intended for educational use only, but its use
 *    is not limited thereto. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

#include <stdlib.h>								// Include standard library header files
#include <avr/io.h>

#include "rs232int.h"							// Include header for serial port class
#include "PID_controller.h"						// Include header for the A/D class
#include "time_stamp.h"							// Class to implement a microsecond timer

/// Define a constant (time between calls) to be used in calculations instead of time_stamps
const float delta_t = 10000;					// time in microseconds between calls
//-------------------------------------------------------------------------------------
/** \brief This constructor sets up a PID controller 
 *  \details This code sets up proportional, integral, and differential gains as well as the 
 *  initializing a time_stamp object to get current RTOS time for delta_t variable. 
 *  @param p_gain Proportional gain
 *  @param i_gain Integral gain
 *  @param d_gain Differential gain
 *  @param p_serial_port Pointer to a serial port to communicate through
 *  @param i_sum_max User-defined maximum integrator sum (magnitude)
 */
PID_controller::PID_controller (float p_gain, float i_gain, float d_gain, emstream* p_serial_port, float i_sum_max)
{
	// Save parameters into object variables
	P_gain = p_gain;
	I_gain = i_gain;
	D_gain = d_gain;
    ptr_to_serial = p_serial_port;
	I_sum_max = i_sum_max;
}

//-------------------------------------------------------------------------------------
/** \brief This method is called to initialize control values
 *  \details This code initializes the controller by resetting the integrator sum and setting
 *  a position_old value for the first run through UpdatePID.
 */
void PID_controller::InitializePID (float actual_value)
{
	I_sum = 0;
	position_old = actual_value;
	
	// Comment out time values to use constant delta_t
//	current_time.set_to_now ();
//	time_old = current_time.get_microsec ();
}
	
//-------------------------------------------------------------------------------------
/** \brief This method is called to update control values 
 *  \details This code updates controller values by running the error value (actual-desired) through
 *  proportional, integral, and derivative gains.
 *  @return The result of control calculations.
 */
float PID_controller::UpdatePID (float desired_value, float actual_value)
{
	// Find time passed since last run, comment out to use constant delta_t
//	current_time.set_to_now ();
//	uint32_t time_new = current_time.get_microsec ();
//	uint32_t delta_t = time_new-time_old;
	
	// Find error value
	float error = desired_value-actual_value;
	
	// Proportional control
	float pTerm = P_gain*error;
	
	// Integral control
	I_sum += error;
	if(I_sum > I_sum_max)
		I_sum = I_sum_max;
	else if(I_sum < (-1*I_sum_max))
		I_sum = -1*I_sum_max;
	float iTerm = I_gain*I_sum*delta_t/1000000;
	
	// Derivative control
	float dTerm = D_gain*(position_old-actual_value)/delta_t*1000000;
	
//	DBG(ptr_to_serial, dec << "P:	" << pTerm << "	I:	" << iTerm << "	D:	" << dTerm <<endl);
//	DBG(ptr_to_serial, "Isum:	" << I_sum << "	delta_t:	" << delta_t << endl);
	
	// Set for next run
	position_old = actual_value;
//	time_old = time_new;
	
	return (pTerm+iTerm+dTerm);
}


//-------------------------------------------------------------------------------------
/** \brief This method prints information about the PID controller
 *  \details This code prints information about the controller for the user.
 *  It will print the inputted gains, current integrator sum, and current position.
 */
void PID_controller::about()
{
	DBG(ptr_to_serial, "	About PID:" << endl);
	DBG(ptr_to_serial, "		K_p " << dec << P_gain << endl);
	DBG(ptr_to_serial, "		K_i " << dec << I_gain << endl);
	DBG(ptr_to_serial, "		K_d " << dec << D_gain << endl);
	DBG(ptr_to_serial, "		Integrator Sum  	" << dec << I_sum << endl);
	DBG(ptr_to_serial, "		Current Position  	" << dec << position_old << endl);
}