//======================================================================================
/** \file Sensor.h
 *    This file contains the Sensor class. It is the parent class of every sensor
 *    on the ADC board.
 *
 *  Revisions:
 *    \li 11-12-2013 CTC Original file
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

#ifndef _AVR_Sensor_H_
#define _AVR_Sensor_H_

#include <stdlib.h>                         // Prototype declarations for I/O functions
#include <string.h>                         // Functions for C string handling
#include "emstream.h"                       // Header for serial ports and devices
#include "FreeRTOS.h"                       // Header for the FreeRTOS RTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // Header for FreeRTOS queues
#include "semphr.h"                         // Header for FreeRTOS semaphores

//-------------------------------------------------------------------------------------
/** \brief This class holds the Sensor parent class.
 *  \details This initializes functions to be used in the Sensor class.
 */
class Sensor
{
	protected:
        /// The name of the sensor.
        const char* name;
        /// A pointer to a serial port.
        emstream* ptr_to_serial;
        
        
    public:
        // The constructor is used so child classes can set the name of the sensor.
		Sensor (const char*, emstream*);
        // The destructor is protected so you can't instantiate the class.
        ~Sensor (void) {};
        // Returns the name of the sensor.
        const char* getName (void);


}; // end of class Sensor
#endif // _AVR_Sensor_H_