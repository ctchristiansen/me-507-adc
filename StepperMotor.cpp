//======================================================================================
/** \file StepperMotor.cpp
 *    This file contains a StepperMotor class. It includes methods that child
 *    classes will need to implement in order to comply with the standard coding
 *    practices used in the ME 507 Atmospheric Dispersion Corrector project.
 *
 *  Revisions:
 *    \li 12-02-2013 GDG Original file created to implement simple stepper control
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************


#include "emstream.h"                       // Header for serial ports and devices
#include "FreeRTOS.h"                       // Header for the FreeRTOS RTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // Header for FreeRTOS queues
#include "semphr.h"                         // Header for FreeRTOS semaphores
#include "StepperMotor.h"


//-------------------------------------------------------------------------------------
/** \brief This class should run the StepperMotor on an AVR processor.
 *  \details This initializes functions to be used in the StepperMotor class. This constructor
 *  also calls the parent constructor from \c Motor.
 *  @param newName Name to be passed to \c Motor parent constructor
 *  @param motor_num Indicates which motor is being controlled
 *  @param p_serial Pointer to a serial device (port, radio, SD card, etc.) which can
 *                   be used by this task to communicate (default: NULL)
 */
StepperMotor::StepperMotor (const char* newName, uint8_t motor_num, emstream* p_serial) :
    Motor(newName, p_serial)
{
    // Set motor variables from inputs
    motor_number = motor_num;
    
    // Initialize based on motor number
    switch(motor_number)
    {
        case(1):
            p_stepper = new DRV8834_driver (0, 1, p_serial);
            p_calSensor = new IRSensor ("IR1", 1, p_serial);
            break;
        case(2):
            p_stepper = new DRV8834_driver (0, 2, p_serial);
            p_calSensor = new IRSensor ("IR2", 2, p_serial);
            break;
    }
    
    // Stepper has holding torque, no need for brake
    brake_on=0;
}

/** \brief This function turns on the motor.
 * \details This function sets the necessary drivers to on.
 */
void StepperMotor::on (void)
{
//    p_stepper->enable();
}


/** \brief This function turns off the motor.
 * \details This function sets the necessary drivers to off.
 */
void StepperMotor::off (void)
{
 //   p_stepper->disable();
}


/** \brief This function toggles the brake of the motor.
 * \details This function determines if the brake is on or off and toggles the value.
 * This is done by enabling or disabling the coils.
 */
void StepperMotor::brake (void)
{
    switch(brake_on)
    {
        case(1):                    // If brake is on, turn it off
            p_stepper->disable();
            brake_on = 0;
            break;
            
        case(0):                    // If brake is off, turn it on
            p_stepper->enable();
            brake_on = 1;
            break;
    }
}

/** \brief Moves the stepper motor to a position specified
 * \details This function sets the position the motor should move to and then sets power 
 * to move there.  Needs to be called at a regular interval.
 * @param pos Position to move to.
 */
void StepperMotor::goToPos (int32_t pos)
{
    if (position == pos)
    {
        p_stepper->disable();
        return;
    }
    else
    {
        p_stepper->enable();
        if (pos > position)
        {
            p_stepper->step(CW);
            position++;
        }
        else
        {
            p_stepper->step(CCW);
            position--;
        }
    }
}

/** \brief Moves the stepper motor to a velocity specified
 * \details This function sets the velocity the motor should move and then sets power 
 * to move there.  Needs to be called at a regular interval.
 * @param vel Velocity to move to.
 */
void StepperMotor::goToVel (int16_t vel)
{
    // Didn't need it, didn't code it.
}

/** \brief This function returns the position moved by the stepper motor.
 * \details This code reads the position and returns a signed 32-bit value.
 * @return Position of motor in ticks.
 */
int32_t StepperMotor::getPos (void)
{
    return position;
}

/** \brief This function sets the position of the DC motor.
 * \details This code sets the position value to the input value specified.
 * @param pos Position to set.
 */
void StepperMotor::setPos (int32_t pos)
{
    position = pos;
}

/// This function sets the velocity of the stepper motor.
void StepperMotor::setVel (int16_t)
{

}

/** \brief This function calibrates the motor with the IR sensor.
 * \details This code turns on the IR sensor and sets power to move until the IR sensor is
 * triggered. 
 * @return Motor calibration state.
 */
bool StepperMotor::calibrate (void)
{
    static bool first_run = true;
    if (first_run)
    {
        p_calSensor->startCalibration();
        first_run = 0;
    }
    
    bool IR = p_calSensor->isTriggered();
    switch(IR)
    {
        case 0:
            p_stepper->enable();
            p_stepper->step(CW);
            return 0;
            break;
        case 1:
            setPos(0);
            if(brake_on==0)
            {
                p_stepper->disable();
            }
            p_calSensor->endCalibration();
            first_run = 1;
            return 1;
            break;

        default:
            return 0;
            break;
    }
}

/// This function overloads the << operator for printing messages.
emstream& operator<< (emstream& out, const StepperMotor& motor)
{
    out << motor.name << endl;
    return out;
}
