//======================================================================================
/** \file Motor.h
 *    This file contains an abstract motor class. It includes methods that child
 *    classes will need to implement in order to comply with the standard coding
 *    practices used in the ME 507 Atmospheric Dispersion Corrector project.
 *
 *  Revisions:
 *    \li 11-07-2013 CTC Original file
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

// This define prevents this .h file from being included multiple times in a .cpp file
#ifndef _AVR_Motor_H_
#define _AVR_Motor_H_

#include <stdlib.h>                         // Prototype declarations for I/O functions
#include <string.h>                         // Functions for C string handling
#include "emstream.h"                       // Header for serial ports and devices
#include "FreeRTOS.h"                       // Header for the FreeRTOS RTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // Header for FreeRTOS queues
#include "semphr.h"                         // Header for FreeRTOS semaphores

//-------------------------------------------------------------------------------------
/** \brief This class should run the motor driver on an AVR processor. 
 *  \details This initializes functions to be used in the motor driver class. Function on
 *  turns on the motor. Function off turns off the motor. Function brake toggles the brake
 *  on and off. Function set_power sets how fast the motor will spin and in which direction.
 *  Fuction get_port_values prints the values of the ports. Function check_on returns 0 if the
 *  motor is off, 1 if on. Function check_brake returns 0 if the brake is off, 1 if on.
 *  Function about prints information about the motor such as on/off and power.
 */

class Motor
{
	protected:
        /// The name of the motor.
        const char* name;
        /// A pointer to a serial port.
        emstream* ptr_to_serial;
        /// Value if brake is on or off.
        uint8_t brake_on;

    public:
        /// The constructor is used so child classes can set the name of the motor.
		Motor (const char* newName, emstream* new_ptr_to_serial)
		{
            this->name = newName;
            this->ptr_to_serial = new_ptr_to_serial;
		}
		~Motor (void) {};

		/// This function turns on the motor.
		virtual void on (void) = 0;

		/// This function turns off the motor.
		virtual void off (void) = 0;

		/// This function toggles the brake on and off.
		virtual void brake (void) = 0;

		/// This function toggles the brake on and off.
		virtual void goToPos (int32_t) = 0;

		/// This function toggles the brake on and off.
		virtual void goToVel (int16_t) = 0;

		/// This function toggles the brake on and off.
		virtual int32_t getPos (void) = 0;

		/// This function toggles the brake on and off.
		virtual void setPos (int32_t) = 0;

		/// This function toggles the brake on and off.
		virtual void setVel (int16_t) = 0;

		/// This function toggles the brake on and off.
		virtual bool calibrate (void) = 0;
}; // end of class Motor
#endif // _AVR_Motor_H_
