//**************************************************************************************
/** \file task_dc_control.h
 *    This file contains the code for a task class which controls two DC motors. 
 *
 *  Revisions:
 *    \li 12-05-2013 GDG Original file created from task_stepper_control
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

// This define prevents this .h file from being included multiple times in a .cpp file
#ifndef _TASK_DC_CONTROL_H_
#define _TASK_DC_CONTROL_H_

#include <stdlib.h>                            // Prototype declarations for I/O functions
#include <avr/io.h>                            // Header for special function registers

#include "FreeRTOS.h"                        // Primary header for FreeRTOS
#include "task.h"                            // Header for FreeRTOS task functions
#include "queue.h"                            // FreeRTOS inter-task communication queues

#include "frt_task.h"                        // ME405/507 base task class
#include "time_stamp.h"                        // Class to implement a microsecond timer
#include "frt_queue.h"                        // Header of wrapper for FreeRTOS queues
#include "frt_shared_data.h"                // Header for thread-safe shared data

#include "rs232int.h"                        // ME405/507 library for serial comm.

#include "DCMotor.h"


//-------------------------------------------------------------------------------------
/** \brief This task controls the radial position of a motor shaft. 
 *  \details The motor controller task is run using drivers in files \c motor_driver.cpp 
 *  \c encoder_driver.cpp and \c PID_controller.cpp.  The PID controller takes current
 *  values from the encoder and runs control loops to determine new power settings to send
 *  to the motor in order to control the position of the motors shaft. 
 */

class task_dc_control : public frt_task
{
private:
    // No private variables or methods for this class

protected:
    // No protected variables or methods for this class

public:
    // This constructor creates a generic task of which many copies can be made
    task_dc_control (const char*, unsigned portBASE_TYPE, size_t, emstream*);

    // This method is called by the RTOS once to run the task loop for ever and ever.
    void run (void);
};

#endif // _TASK_DC_CONTROL_H_
