//======================================================================================
/** \file DCMotor.h
 *    This file contains a DCMotor class. It includes methods that child
 *    classes will need to implement in order to comply with the standard coding
 *    practices used in the ME 507 Atmospheric Dispersion Corrector project.
 *
 *  Revisions:
 *    \li 12-05-2013 GDG Original file taken from StepperMotor
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

#ifndef _AVR_DCMotor_H_
#define _AVR_DCMotor_H_

#include <stdlib.h>                         // Prototype declarations for I/O functions
#include <string.h>                         // Functions for C string handling
#include "emstream.h"                       // Header for serial ports and devices
#include "FreeRTOS.h"                       // Header for the FreeRTOS RTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // Header for FreeRTOS queues
#include "semphr.h"                         // Header for FreeRTOS semaphores
#include "rs232int.h"                       // ME405/507 library for serial comm.
#include "frt_queue.h"                      // Header for generic RTOS queue class
#include "frt_text_queue.h"                 // Header for RTOS text queue class
#include "frt_shared_data.h"                // Header for thread-safe shared data

#include "Motor.h"                          // Header for Interface file
#include "DRV8833_driver.h"                 // Header for Stepper Driver Chip
#include "encoder_driver.h"                 // Header for encoders
#include "PID_controller.h"                 // Header for PID controller class
#include "IRSensor.h"						// Header for IR sensor class


//-------------------------------------------------------------------------------------
/** \brief This class should run the motor driver on an AVR processor.
 *  \details This initializes functions to be used in the motor driver class. Function on
 *  turns on the motor. Function off turns off the motor. Function brake toggles the brake
 *  on and off. Function set_power sets how fast the motor will spin and in which direction.
 *  Fuction get_port_values prints the values of the ports. Function check_on returns 0 if the
 *  motor is off, 1 if on. Function check_brake returns 0 if the brake is off, 1 if on.
 *  Function about prints information about the motor such as on/off and power.
 */

class DCMotor : public Motor
{
    protected:
        /// The instance of DCMotor being called: 1 or 2.
        uint8_t motor_number;

        /// Contains a pointer to the DC Driver associated
        DRV8833_driver* p_motor;

        /// Contains a pointer to the encoder driver associated
        encoder_driver* p_encoder;

        /// Contains a pointer to the calibration sensor (IR)
        IRSensor* p_calSensor;

        /// Pointer to position PID controller
        PID_controller* p_PID_position;


    public:
        DCMotor (const char*, uint8_t, emstream*);
        virtual ~DCMotor (void) {};

        // This function turns on the motor.
        virtual void on (void);

        // This function turns off the motor.
        virtual void off (void);

        // This function toggles the brake on and off.
        virtual void brake (void);

        // This sets the position to move to
        virtual void goToPos (int32_t);

        // This sets the velocity to move to
        virtual void goToVel (int16_t);

        // This returns the position from the encoder
        virtual int32_t getPos (void);

        // This sets the position of the encoder
        virtual void setPos (int32_t);

        // This sets the velocity to move at
        virtual void setVel (int16_t);

        // This calibrates the motor
        virtual bool calibrate (void);

        // Directly set the power output of the motor.
        void set_power (int16_t);

        // Initialize position PID
        void initPosPID (void);

        // Initialize velocity PID
        void initVelPID (void);

        // Overloaded << operator for printing functionality.
        friend emstream& operator<< (emstream&, const DCMotor&);

}; // end of class DCMotor
#endif // _AVR_DCMotor_H_
