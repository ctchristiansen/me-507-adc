//**************************************************************************************
/** \file task_user.cpp
 *    This file contains source code for a user interface task for the atmospheric
 *    dispersion corrector board.
 *
 *  Revisions:
 *    \li 12-02-2013 GDG Original file taken from ME405 code. Adapted to test basic
 *                       functions of StepperMotor.
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************
#include <stdlib.h>                         // Header for standard libraryz
#include <avr/io.h>                         // Port I/O for SFR's
#include <avr/wdt.h>                        // Watchdog timer header

#include "shared_data_sender.h"             // Header for shared data
#include "shared_data_receiver.h"           // Header for shared data
#include "task_user.h"                      // Header for this file
#include "errno.h"                          // Header for errno
#include <avr/pgmspace.h>                   // Header for pgmspace


//-------------------------------------------------------------------------------------
/** This constructor creates a new user task. Its main job is to call the
 *  parent class's constructor which does most of the work.
 *  @param a_name A character string which will be the name of this task
 *  @param a_priority The priority at which this task will initially run (default: 0)
 *  @param a_stack_size The size of this task's stack in bytes 
 *                      (default: configMINIMAL_STACK_SIZE)
 *  @param p_ser_dev Pointer to a serial device (port, radio, SD card, etc.) which can
 *                   be used by this task to communicate (default: NULL)
 */

task_user::task_user (const char* a_name, 
                      unsigned portBASE_TYPE a_priority, 
                      size_t a_stack_size,
                      emstream* p_ser_dev
                     )
    : frt_task (a_name, a_priority, a_stack_size, p_ser_dev)
{
    // Initialize instance variables
    thetaOffsetX10 = 0;
}


//-------------------------------------------------------------------------------------
/**
 * \brief This method handles the communication to and from the PC.
 * \details This method continually checks for data from the serial port. If a
 *          charater comes in it sends it to the receive() method. It also checks
 *          the serial queue and writes waiting data to the serial port.
 */
void task_user::run (void)
{
    char char_in;                           // Character read from serial device
    time_stamp a_time;                      // Holds the time so it can be displayed
    
    uint16_t pos_calc;
    
    // Initialize variables used in the receive function.
    index = 0;
    inData[index] = NULL;
    started = false;

    // This is an infinite loop; it runs until the power is turned off. There is one 
    // such loop inside the code for each task
    for (;;)
    {
        if (p_serial->check_for_char ())
        {
            // Read in one character at a time from the serial port and send it
            // to the receive() function to process.
            receive(p_serial->getchar());
        }
        if (print_ser_queue->check_for_char ())
        {
            // Check to see if any data needs to be written to the serial port and do it.
            p_serial->putchar(print_ser_queue->getchar());
        }
        
        // No matter the state, wait for approximately a millisecond before we 
        // run the loop again. This gives lower priority tasks a chance to run
        vTaskDelay (configMS_TO_TICKS (1));
    }
}


/**
 * \brief This method 
 * \details This method takes each character received from the serial port and
 * converts them into a command string as long as they follow the correct
 * command format. The correct command format begins with '<' (SOC) and the command
 * type, ends with '>' (EOC), and has data in between separated by commas.
 * (Ex. '<1,1234,1234>'). After a command has been received, it is sent to
 * commandParser to analyze.
 * @param inChar A single character received by the serial port
 */
void task_user::receive (char inChar)
{
    // We first wait until the start character is received.
    if (inChar == SOC) 
    {
       index = 0;
       inData[index] = NULL;
       started = true;
    }
    // When we get the end character, we then parse the command
    // and reset the inData string.
    else if (inChar == EOC) 
    {
        parseInput(inData);
        index = 0;
        inData[index] = NULL;
        started = false;
    }
    // If we've gotten the start character, and the command's length
    // is not too large for the array, add the character to the 
    // inData string.
    else if (started && (index < MAX_COMMAND_SIZE))
    {
        inData[index] = inChar;
        index++;
        inData[index] = NULL;
    }
    // We're here because we got a character before a start character 
    // or because the command was too long for us to handle. Either
    // way, clear the string and wait for the start of a new command.
    else 
    {
        index = 0;
        inData[index] = NULL;
        started = false;
    }
}

/**
 * \brief This method takes the command string and determines what to do with the data.
 * \details This method takes the command string and determines what to do with the data.
 * @param input A null-terminated character array representing a command sent from
 *              the PC.
 */
void task_user::parseInput (char input[])
{
    // Create a variable for strtok_r to use. This allows the tokenizer method 
    // to be reentrant and therefore thread-safe. Also create a pointer that allows
    // us to check strtol for errors.
    char* saveptr;
    char* error;
    
    // Tokenize the string and store in a new variable. The first token is
    // a string that corresponds with the Commands enum received (an int).
    // To use a switch, you must convert the string to an int. We use strtol 
    // since it has error handling abilities.
    char* cmdStr = strtok_r (input,",", &saveptr);
    errno = 0;
    int cmd = strtol (cmdStr, &error, 10);
    if (*error != NULLCHAR)
    {
        // If there was an error, we need to let the PC know. Set cmd to the
        // error state and we will pass along the error message.
        error = "String error";
        cmd = ERROR_STATE;
    }
    else if (errno == ERANGE)
    {
        // The integer was out of range. Set cmd to the error state and we 
        // will pass along the error message.
        error = "ERANGE error";
        cmd = ERROR_STATE;
    }
    
    // Create places to put the data here so we don't have to declare them in  
    // every case. Make sure there are enough data variables for the largest case.
    char* data1 = NULLCHAR;
    char* data2 = NULLCHAR;
    char* data3 = NULLCHAR;
    char data4[10];
    char data5[10];
    char string[25];
    int thetaX10;
    int deltaX10;
    
    switch (cmd)
    {
    // We can now read in the data from the message. Each case could have a
    // different amount of data sent, so read in data on a per case basis. 
    // Each piece of data is seperated by a comma, so keep calling strtok_r
    // to get the next tokens.
    case ASK_ATTITUDE:
        data1 = strtok_r (NULL, ",", &saveptr);
        data2 = strtok_r (NULL, ",", &saveptr);
        *print_ser_queue << SOC << cmd << EOC;
        break;
        
    case ASK_POSITION:
        data1 = strtok_r (NULL, ",", &saveptr);
        data2 = strtok_r (NULL, ",", &saveptr);

        thetaX10 = strtol(data1, &error, 10);
        deltaX10 = strtol(data2, &error, 10);

        p_share_prism1PosDesX10->put((thetaX10 + deltaX10/2 + thetaOffsetX10));
        p_share_prism2PosDesX10->put((thetaX10 - deltaX10/2 + thetaOffsetX10));
	
        *print_ser_queue << SOC << cmd << EOC;
        break;
        
    case GIVE_CUR_POS:
    {
        int temp1 = p_share_prism1PosActX10->get();
        int temp2 = p_share_prism2PosActX10->get();
        deltaX10 = temp1 - temp2;
        thetaX10 = (temp1 + temp2)/2 - thetaOffsetX10;
        *print_ser_queue << SOC << cmd << ',';
        *print_ser_queue << thetaX10 << ','; 
        *print_ser_queue << deltaX10 << EOC;
    }
        break;
        
    case ASK_CALIBRATE:
        flag_motor1cal->put(1);
        flag_motor2cal->put(1);
        // Note, motor control task will respond when calibration is finished
        break;
        
    case ASK_THETA_OFFSET:
    {
        int temp0 = thetaOffsetX10;
        data1 = strtok_r (NULL, ",", &saveptr);
        thetaOffsetX10 = strtol(data1, &error, 10);
        
        int temp1 = p_share_prism1PosDesX10->get();
        int temp2 = p_share_prism2PosDesX10->get();
        
        p_share_prism1PosDesX10->put(temp1 + thetaOffsetX10 - temp0);
        p_share_prism2PosDesX10->put(temp2 + thetaOffsetX10 - temp0);
	
        *print_ser_queue << SOC << cmd << EOC;
    }
        break;
        
    case ERROR_STATE:
        send (ERROR_STATE, error);
        break;
        
    default:
        send (ERROR_STATE, "No such case exists");
        break;
    }
}

/**
 * \brief This function takes a command type to send to the PC.
 * \details This function takes a command type to send to the PC. This method is
 * meant for ASK type commands where no data needs to be sent.
 * If the command doesn't need data, the second input should be NULL.
 * @param inCommand A Commands enum that determines what to send to the PC.
 */
void task_user::send (Commands inCommand)
{
    switch (inCommand)
    {
    case (ASK_ATTITUDE):
        *print_ser_queue << SOC << ASK_ATTITUDE << EOC;
        break;

    case (ASK_POSITION):
        *print_ser_queue << SOC << ASK_POSITION << EOC;
        break;

    default:
        send (ERROR_STATE, "No such case exists in send()");
        break;
    }
}

// 
/**
 * \brief This function takes a command type to send to the PC.
 * \details This function takes a command type and data to send to the PC. This
 * method is meant for GIVE type commands where the PC asked for data.
 * The data should be included as a single string with each piece of
 * data separated by commas (ex. data = "12,345"). If for some reason
 * the command doesn't need to send data, the second input should be NULL.
 * @param inCommand A Commands enum that determines what to send to the PC.
 * @param data A data string to be sent to along with the command to the PC.
 *             It must include any commas necessary to separate the data.
 */
void task_user::send (Commands inCommand, char data[])
{
    switch (inCommand)
    {
    case (GIVE_CUR_POS):
        *print_ser_queue << SOC << GIVE_CUR_POS << ',' << data << EOC;
        break;

    case (ERROR_STATE):
        *print_ser_queue << SOC << ERROR_STATE << EOC;
        break;

    default:
        *print_ser_queue << SOC << ERROR_STATE << ',' << data << EOC;
        break;
    }
}
