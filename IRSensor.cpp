//======================================================================================
/** \file IRSensor.cpp
 *    This file contains the ITSensor class. It contains code that uses an IR receiver
 *    and transmitter as a calibration sensor for the ADC prism location. It is a child
 *    of the CalibrationSensor class.
 *
 *  Revisions:
 *    \li 11-07-2013 CTC Original file
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

#include "IRSensor.h"                       // Header for IRSensor class

//-------------------------------------------------------------------------------------
/** \brief This class creates an IRSensor object that is used as a calibration sensor.
 *  \details This class creates an IRSensor object that is used as a calibration sensor.
 *           It is a child of CalibrationSensor and implements the methods necessary
 *           to calibrate the ADC prisms' positions.
 */
IRSensor::IRSensor (const char* newName, uint8_t sensor, emstream* p_serial) :
    CalibrationSensor(newName, p_serial)
{
    sensorNumber = sensor;
	// set emitter pin as output and initialize off
    DDRB |= (1<<PB2);
    PORTB &= ~(1<<PB2);
	
	// Set receiver pins as inputs
    DDRB &= ~((1<<PB3) | (1<<PB4));
}

/** \brief This method tells the sensor to begin calibrating the prisms.
 *  \details This method tells the sensor to begin calibrating the prisms.
 *           It does so by supplying power to the two IR transmitters.
 */
void IRSensor::startCalibration (void)
{
	PORTB |= (1<<PB2);
}

/** \brief This method tells the sensor to end calibrating the prisms.
 *  \details This method tells the sensor to end calibrating the prisms.
 *           It does so by cutting power to the two IR transmitters.
 */
void IRSensor::endCalibration (void)
{
    PORTB &= ~(1<<PB2);
}

/** \brief This method returns true if the sensor is currently triggered.
 *  \details This method returns true if the sensor is currently triggered.
 *           It checks the IR receivers to check if they have been triggered.
 */
bool IRSensor::isTriggered (void)
{
	if (sensorNumber == 1)
		return (PINB & (1<<PB3)) ? true : false;

	else if (sensorNumber == 2)
		return (PINB & (1<<PB4)) ? true : false;
    
    else
        return false;
}

/** \brief This method allows you to print an IRSensor object.
 *  \details This method allows you to print an IRSensor object.
 */
emstream& operator<< (emstream& out, const IRSensor& sensor)
{
    out << sensor.name << endl;
    return out;
}
