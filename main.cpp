//*************************************************************************************
/** \file main.cpp
 *    This file contains the main() code for a program which runs the ADC control board. 
 *    It is capable of controlling two motors simultaniously, either stepper or DC, and 
 *    aligning them to position two prisms to correct the dispersion caused by them
 *    the atmosphere. Users interact with the software through \c task_user which 
 *    communicates meaningful data to the \c task_dc_control or \c  task_stepper_control
 *    tasks. 
 *
 *  Revisions:
 *    \li 12-05-2013 GDG Original file created to support task_user and task_stepper_control.
 *    \li 12-05-2013 GDG Code adjusted to be interchangeable between task_stepper_control
 *                        and task_dc_control.
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************



 /** \mainpage Inter-task communication 
  *  The task diagram for this program is shown below. 
  *  \dot
  *  digraph task_diagram {
  *  node [ shape = box, fontname = FreeSans, fontsize = 10 ];
  * 	task_user   [ label = "User Interface\nPri: 1\n1 ms" URL = "\ref task_user" ];
  * 	task_motor_control	[ label = "Motor Control\nPri: 2\n10 ms" URL = "\ref task_dc_control" ];
  *  edge [ arrowhead = normal, style = solid, fontname = FreeSans, fontsize = 10 ];
  * 	task_user	-> task_motor_control [ label = "flag_motor1cal\nbool" ];
  * 	task_user	-> task_motor_control [ label = "flag_motor2cal\nbool" ];
  * 	task_user	-> task_motor_control [ label = "p_share_prism1PosDesX10\nuint16_t" ];
  * 	task_user	-> task_motor_control [ label = "p_share_prism2PosDesX10\nuint16_t" ];
  * 	task_motor_control	-> task_user [ label = "p_share_prism1PosActX10\nuint16_t" ];
  * 	task_motor_control	-> task_user [ label = "p_share_prism2PosActX10\nuint16_t" ];
  *  }
  *  \enddot
  */

#include <stdlib.h>                         // Prototype declarations for I/O functions
#include <avr/io.h>                         // Port I/O for SFR's
#include <avr/wdt.h>                        // Watchdog timer header
#include <string.h>                         // Functions for C string handling

#include "FreeRTOS.h"                       // Primary header for FreeRTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // FreeRTOS inter-task communication queues
#include "croutine.h"                       // Header for co-routines and such

#include "rs232int.h"                       // ME405/507 library for serial comm.
#include "time_stamp.h"                     // Class to implement a microsecond timer
#include "frt_task.h"                       // Header of wrapper for FreeRTOS tasks
#include "frt_text_queue.h"                 // Wrapper for FreeRTOS character queues
#include "frt_queue.h"                      // Header of wrapper for FreeRTOS queues
#include "frt_shared_data.h"                // Header for thread-safe shared data
#include "shares.h"                         // Global ('extern') queue declarations

#include "task_user.h"                      // Header for user interface task
#include "task_dc_control.h"                // Header for dc control task
#include "task_stepper_control.h"           // Header for stepper control task


// Declare the queues which are used by tasks to communicate with each other here. 
// Each queue must also be declared 'extern' in a header file which will be read 
// by every task that needs to use that queue. The format for all queues except 
// the serial text printing queue is 'frt_queue<type> name (size)', where 'type' 
// is the type of data in the queue and 'size' is the number of items (not neces-
// sarily bytes) which the queue can hold

/** This is a print queue, descended from \c emstream so that things can be printed 
 *  into the queue using the "<<" operator and they'll come out the other end as a 
 *  stream of characters. It's used by tasks that send things to the user interface 
 *  task to be printed. 
 */
frt_text_queue* print_ser_queue;

/* These are the necessary shares file driving two motor drivers. Details can be found 
   in the shares.h file.
 */
shared_data<bool>* flag_motor1cal;
shared_data<bool>* flag_motor2cal;
shared_data<uint16_t>* p_share_prism1PosDesX10;
shared_data<uint16_t>* p_share_prism2PosDesX10;
shared_data<uint16_t>* p_share_prism1PosActX10;
shared_data<uint16_t>* p_share_prism2PosActX10;


//=====================================================================================
/** The main function sets up the RTOS.  Some test tasks are created. Then the 
 *  scheduler is started up; the scheduler runs until power is turned off or there's a 
 *  reset.
 *  @return This is a real-time microcontroller program which doesn't return. Ever.
 */

int main (void)
{
        // Disable the watchdog timer unless it's needed later. This is important because
        // sometimes the watchdog timer may have been left on...and it tends to stay on
        MCUSR = 0;
        wdt_disable ();

        // Configure a serial port which can be used by a task to print debugging infor-
        // mation, or to allow user interaction, or for whatever use is appropriate.  The
        // serial port will be used by the user interface task after setup is complete and
        // the task scheduler has been started by the function vTaskStartScheduler()
        rs232* ser_port = new rs232 (9600, 0);
        *ser_port << clrscr << PMS ("ADC DC test program") << endl;

        // Create the queues and other shared data items here
        print_ser_queue = new frt_text_queue (32, ser_port, 10);
        
        // Flags from User to Motor Control
        flag_motor1cal = new shared_data<bool>;
        flag_motor2cal = new shared_data<bool>;
        
        // Shares indicating desired vs actual motor positions
        p_share_prism1PosDesX10 = new shared_data<uint16_t>;
        p_share_prism2PosDesX10 = new shared_data<uint16_t>;
        p_share_prism1PosActX10 = new shared_data<uint16_t>;
        p_share_prism2PosActX10 = new shared_data<uint16_t>;
        
        // Shares necessary to run encoder driver in DC Motor
        p_share_encoder1pos   = new shared_data<int32_t>;
        p_share_encoder2pos   = new shared_data<int32_t>;
        p_share_encoder1error = new shared_data<uint16_t>;
        p_share_encoder2error = new shared_data<uint16_t>;


        // The user interface is at low priority; it could have been run in the idle task
        // but it is desired to exercise the RTOS more thoroughly in this test program
        new task_user ("UserInt", task_priority (1), 260, ser_port);

        // Create a task which controls two stepper motors by either calibrating or moving
        // motors to desired angles.  Gear constants included in task.
        new task_dc_control ("DCControl", task_priority (2), 280, ser_port);
        
        // Create a task which controls two stepper motors by either calibrating or moving
        // motors to desired angles.  Gear constants included in task.
//        new task_stepper_control ("StepControl", task_priority (2), 280, ser_port);
        
        // Here's where the RTOS scheduler is started up. It should never exit as long as
        // power is on and the microcontroller isn't rebooted
        vTaskStartScheduler ();
}


