//*************************************************************************************
/** \file shares.h
 *    This file contains extern declarations for queues and other inter-task data
 *    communication objects used in a ME405/507/FreeRTOS project. 
 *
 *  Revisions:
 *    \li 09-30-2012 JRR Original file was a one-file demonstration with two tasks
 *    \li 10-05-2012 JRR Split into multiple files, one for each task plus a main one
 *    \li 10-29-2012 JRR Reorganized with global queue and shared data references
 *
 *  License:
 *		This file is copyright 2012 by JR Ridgely and released under the Lesser GNU 
 *		Public License, version 2. It intended for educational use only, but its use
 *		is not limited thereto. */
/*		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 *		AND	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * 		IMPLIED 	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * 		ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * 		LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 * 		TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
 * 		OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * 		CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 * 		OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 * 		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

// This define prevents this .h file from being included multiple times in a .cpp file
#ifndef _SHARES_H_
#define _SHARES_H_


//-------------------------------------------------------------------------------------
// Externs:  In this section, we declare variables and functions that are used in all
// (or at least two) of the files in the data acquisition project. Each of these items
// will also be declared exactly once, without the keyword 'extern', in one .cpp file
// as well as being declared extern here. 

// This queue allows tasks to send characters to the user interface task for display.
extern frt_text_queue* print_ser_queue;


/// Calibration flag of Motor 1
extern shared_data<bool>* flag_motor1cal;

/// Calibration flag of Motor 2
extern shared_data<bool>* flag_motor2cal;

/// Desired Position of Prism 1 in 10th of deg
extern shared_data<uint16_t>* p_share_prism1PosDesX10;

/// Desired Position of Prism 2 in 10th of deg
extern shared_data<uint16_t>* p_share_prism2PosDesX10;

/// Actual Position of Prism 1
extern shared_data<uint16_t>* p_share_prism1PosActX10;

/// Actual Position of Prism 2
extern shared_data<uint16_t>* p_share_prism2PosActX10;

/// Motor position in ticks from encoder 1
extern shared_data<int32_t>*  p_share_encoder1pos;

/// Motor position in ticks from encoder 2
extern shared_data<int32_t>*  p_share_encoder2pos;

/// Counter for errors occured in encoder 1
extern shared_data<uint16_t>* p_share_encoder1error;

/// Counter for errors occured in encoder 2
extern shared_data<uint16_t>* p_share_encoder2error;


//-------------------------------------------------------------------------------------
/** The Commands enum contains the commands available for the CommandParser.
*   It is outside of any task or class so that you can use 'ASK_ATTITUDE' 
*   instead of 'CommandParser::ASK_ATTITUDE'. While this would probably be a 
*   more transparent way to do it, it clutters the code.
*/
enum Commands {
ERROR_STATE,
ASK_ATTITUDE,
ASK_POSITION,
GIVE_CUR_POS,
ASK_ADC_CONSTANTS,
ASK_AUTO,
ASK_CALIBRATE,
ASK_THETA_OFFSET
};






#endif // _SHARES_H_
