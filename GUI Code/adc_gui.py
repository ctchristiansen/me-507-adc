"""
Created on Nov 19, 2013

@author: Corey
"""

import sys
import serial
import time
import Queue
import ConfigParser
import threading
import distutils

try:
	import pygtk
	pygtk.require("2.0")
except:
	pass
try:
	import gtk
except:
	print("GTK Not Availible")
	sys.exit(1)
	
	
def enum(*sequential, **named):
	""" Python doesn't have enums, so let's make our own. """
	enums = dict(zip(sequential, range(len(sequential))), **named)
	return type('Enum', (), enums)

Commands = enum(
""" This Commands enum should contain the same states, and in the
same order, as the CommandParser.cpp class on the board.

"""
	# Board tells GUI it ran into an error
	'ERROR_STATE',
	# Board asks GUI for the attitude of the telescope
	'ASK_ATTITUDE', # <X,1234,1234>
	# GUI sends the position the board needs to spin the prisms to
	'ASK_POSITION', # <X,1234,1234>
	# GUI asks board for the prisms' current position
	'GIVE_CUR_POS', # <X,1234,1234>
	# GUI sends necessary constants to do the prism angle math
	'ASK_ADC_CONSTANTS', # <X,1234,1234>
	# GUI sends 0 for manual mode and 1 for auto mode
	'ASK_AUTO', # <X,0/1>
	# GUI sends the command to calibrate, board sends it back when finished
	'ASK_CALIBRATE', # <X>
	# GUI sends the offset for theta
	'ASK_THETA_OFFSET', # <X,1234>
	)

class AutoThread(threading.Thread):
	def __init__(self, serialQueue, thetaDot, deltaDot):
		""" Create a thread that automatically sends new positions to
		the board ten times a second.

		Keyword arguments:
		serialQueue -- The queue that all threads put data they want the
						serial thread to print.
		thetaDot -- The amount theta will increment each time a command is sent.
		deltaDot -- The amount delta will increment each time a command is sent.
		
		"""
		threading.Thread.__init__(self)
		self.isStopped = False
		self.thetaDot = thetaDot
		self.deltaDot = deltaDot
		self.serialQueue = serialQueue
		
	def stop(self):
		""" Stop the thread's execution.

		This method works because the run method checks the isStopped
		variable every time through the while loop.
		
		"""
		self.isStopped = True

	def run(self):
		""" Send an auto-incrementing position to the board.

		The command that sends the position is put in a queue so that the
		serial thread can do all of the serial work for the gui. It breaks
		out of the while loop and ends when stop() is call.
		
		"""
		theta = 0
		delta = 0
		while not self.isStopped:
			theta += self.thetaDot
			delta += self.deltaDot
			temp = '<'
			temp += str(Commands.ASK_POSITION)
			temp += ','
			temp += str(int(10*theta))
			temp += ','
			temp += str(int(10*delta))
			temp += '>'
			print(temp)
			self.serialQueue.put_nowait(temp)
			time.sleep(.1)
			
class AdcGui:
	wTree = gtk.Builder()
	
	def __init__(self):
		""" Initialize all of the objects in the GUI and create
		all instance variables and threads used.
		
		"""
		self.builder = gtk.Builder()
		self.builder.add_from_file("adc_gui.glade")
		self.window = self.builder.get_object ("mainWindow")
		self.window.set_title("ADC Command Center")
		if self.window:
			self.window.connect("destroy", gtk.main_quit)
		self.commandView = self.builder.get_object ("commandView")
		self.commandViewBuffer = self.commandView.get_buffer()
		self.errorView = self.builder.get_object ("errorView")
		self.errorViewBuffer = self.errorView.get_buffer()
		self.thetaOffsetEntry = self.builder.get_object ("thetaOffsetEntry")
		self.thetaEntry = self.builder.get_object ("thetaEntry")
		self.deltaEntry = self.builder.get_object ("deltaEntry")
		self.currentThetaEntry = self.builder.get_object ("currentThetaEntry")
		self.currentDeltaEntry = self.builder.get_object ("currentDeltaEntry")

		self.autoButton = self.builder.get_object ("autoButton")
		self.manualButton = self.builder.get_object ("manualButton")
		
		dic = { 
			"on_autoButton_clicked" : self.autoButtonClicked,
			"on_manualButton_clicked" : self.manualButtonClicked,
			"on_setOffsetButton_clicked" : self.setOffsetButtonClicked,
			"on_setPositionButton_clicked" : self.setPositionButtonClicked,
			"on_calibrateButton_clicked" : self.calibrateButtonClicked,
			"on_windowMain_destroy" : self.quit,
		}
		self.builder.connect_signals(dic)
		
		# Read in the config file containing important ADC stuff
		self.config = ConfigParser.ConfigParser()
		self.config.read("config.ini")
		
		#Create variables and stuff
		self.serialQueue = Queue.Queue();
		self.isCalibrating = False
		self.inData = ''
		self.started = False
		self.MAX_COMMAND_SIZE = 50
		self.port = self.config.get('SerialInfo', 'SerialPort')
		
		# Create all the threads
		self.serialThread = threading.Thread(target=self.SerialThread)
		self.serialThread.daemon = True
		self.serialThread.shutdown = False
		self.curPosThread = threading.Thread(target=self.CurPosThread)
		self.curPosThread.daemon = True
		self.curPosThread.shutdown = False
		self.autoThread = AutoThread(self.serialQueue,.1,0)
		self.autoThread.daemon = True
		
		try:
			self.ser = serial.Serial(port = self.port, baudrate=9600, bytesize=8, parity='N',
									stopbits=1, timeout=None, xonxoff=0, rtscts=0)
			self.ser.close()
		except: # SerialException:
			print("No serial connection")
			sys.exit(1)
		
		self.serialThread.start()
		self.curPosThread.start()
		
	def CurPosThread(self):
		""" Periodically ask for the prisms' current positions. """
		getPos = '<'
		getPos += str(Commands.GIVE_CUR_POS)
		getPos += '>'
		while not self.curPosThread.shutdown:
			if self.ser.isOpen():
				self.serialQueue.put_nowait(getPos)
			time.sleep(1)
		
	def SerialThread(self):
		""" Handle all of the serial activity for the GUI.
		
		This thread opens the serial port and sends and receives serial data.
		When data is received, it passes it to the receive() method. 
		
		"""
		if not self.ser.isOpen():
			self.ser.open()
		while not self.serialThread.shutdown:
			if self.ser.isOpen():
				if self.ser.inWaiting() > 0:
					self.receive(self.ser.read().strip())
				if not self.serialQueue.empty():
					self.ser.write(self.serialQueue.get_nowait())
					self.ser.flush()

	def receive(self, inChar):
		""" Handle the characters that are sent from the board.
		
		Keyword arguments:
		inChar - A single character that has been received from the serial port.
		
		This method gets one character at a time and stores them in a
		string as long as they follow the correct command format. Commands
		start with a '<' and end with a '>'. In between is the data to be
		sent, separated by commas. When a '>' is recieved, the command is
		sent to parseInput() for processing.
		
		"""
		if inChar == '<':
			self.inData = ''
			self.started = True
		elif inChar == '>':
			if not self.inData == '':
				self.parseInput(self.inData)
			self.inData = ''
			self.started = False
		elif self.started and len(self.inData) < self.MAX_COMMAND_SIZE:
			self.inData += inChar
		else:
			self.inData = ''
			self.started = False
		
	def parseInput(self, newInput):
		""" Parses a command string sent from the board.
		
		Keyword arguments:
		newInput - A command string that has been received from the board. The
					'<' and '>' should not be included in the string.
		
		This method checks the command type sent by the board and acts accordingly.
		
		"""
		data = newInput.split(',')
		print(data)

		# The first parameter of a command is always the command type
		cmd = int(data[0])
		
		if cmd == Commands.ASK_ATTITUDE:
			pass
		elif cmd == Commands.ASK_POSITION:
			pass
		elif cmd == Commands.GIVE_CUR_POS:
			# Update the gui with the new prism positions
			self.currentThetaEntry.set_text(str(float(data[1])/10))
			self.currentDeltaEntry.set_text(str(float(data[2])/10))
		elif cmd == Commands.ASK_ADC_CONSTANTS:
			pass
		elif cmd == Commands.ASK_AUTO:
			pass
		elif cmd == Commands.ASK_CALIBRATE:
			pass
		elif cmd == Commands.ASK_THETA_OFFSET:
			pass
		elif cmd == Commands.ERROR_STATE:
			self.errorViewBuffer.set_text("Errawr")
		else:
			pass
			
	def autoButtonClicked(self, widget):
		""" Handle the manual button action.

		When the manual button is pressed it stops the autoThread

		"""
		# Create an AutoThread to handle the position updates
		if self.autoButton.get_active():
			self.manualButton.set_active(False)
			self.autoThread = AutoThread(self.serialQueue,.1,0)
			self.autoThread.daemon = True
			self.autoThread.start()
		else:
			self.autoThread.stop()

	def	calcTheta():
		""" Calculate the necessary theta for the prisms.
		
		This method will eventually calculate the required theta based
		on the telescope position, temperature, prism types, etc.
		
		"""
		return 10	
		pass
		
	def	calcDelta():
		""" Calculate the necessary delta for the prisms.

		This method will eventually calculate the required delta based
		on the telescope position, temperature, prism types, etc.

		"""
		return 20
		pass
		
	def manualButtonClicked(self, widget):
		""" Handle the manual button action.
		
		When the manual button is pressed it stops the autoThread, if running,
		and allows the user to set the prisms' position manually.
		
		"""
		if self.autoButton.get_active():
			self.autoButton.set_active(False)
			self.autoThread.stop()
			
	def setOffsetButtonClicked(self, widget):
		""" Handle the set offset button action.
		
		When the set offset button is pressed it sends a command to the
		board 
		
		"""
		temp = '<'
		temp += str(Commands.ASK_THETA_OFFSET)
		temp += ','
		temp += str(int(10*float(self.thetaOffsetEntry.get_text())))
		temp += '>'
		self.serialQueue.put_nowait(temp)
		
	def setPositionButtonClicked(self, widget):
		""" Handle the set position button action.
		
		When the set position button is pressed it sends a command
		to the board telling what theta and delta to go to.
		
		"""
		if self.manualButton.get_active() and self.ser.isOpen():
			print(self.thetaEntry.get_text())
			temp = '<'
			temp += str(Commands.ASK_POSITION)
			temp += ','
			temp += str(int(10*float(self.thetaEntry.get_text())))
			temp += ','
			temp += str(int(10*float(self.deltaEntry.get_text())))
			temp += '>'
			self.serialQueue.put_nowait(temp)
		
	def calibrateButtonClicked(self, widget):
		""" Handle the calibrate button action.

		When the calibrate button is pressed a command is sent to the board
		telling it to calibrate. When the calibration is finished the board
		will send the command back.

		"""
		temp = '<'
		temp += str(Commands.ASK_CALIBRATE)
		temp += '>'
		self.serialQueue.put_nowait(temp)
  
	def quit(self, widget):
		""" Handle the case when the GUI is closed.

		When the GUI is closed, all of the threads should be stopped.
		The serial port should also be closed her so it doesn't block
		new connections.

		"""
		self.SerialThread.shutdown = True
		self.curPosThread.shutdown = True
		self.autoThread.stop()
		self.ser.close()
		sys.exit(0)
		
# Start the GUI
gtk.gdk.threads_init()
AdcGui = AdcGui()
AdcGui.window.show()
gtk.main()
