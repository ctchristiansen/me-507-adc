//======================================================================================
/** \file StepperMotor.h
 *    This file contains a StepperMotor class. It includes methods that child
 *    classes will need to implement in order to comply with the standard coding
 *    practices used in the ME 507 Atmospheric Dispersion Corrector project.
 *
 *  Revisions:
 *    \li 12-02-2013 GDG Original file created to implement simple stepper control
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

#ifndef _AVR_StepperMotor_H_
#define _AVR_StepperMotor_H_

#include <stdlib.h>                         // Prototype declarations for I/O functions
#include <string.h>                         // Functions for C string handling
#include "emstream.h"                       // Header for serial ports and devices
#include "FreeRTOS.h"                       // Header for the FreeRTOS RTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // Header for FreeRTOS queues
#include "semphr.h"                         // Header for FreeRTOS semaphores
#include "rs232int.h"                       // ME405/507 library for serial comm.
#include "frt_queue.h"                      // Header for generic RTOS queue class
#include "frt_text_queue.h"                 // Header for RTOS text queue class
#include "frt_shared_data.h"                // Header for thread-safe shared data

#include "Motor.h"                          // Header for Interface file
#include "DRV8834_driver.h"                 // Header for Stepper Driver Chip
#include "IRSensor.h"                       // Header for the IR sensor class


//-------------------------------------------------------------------------------------
/** \brief This class should run the motor driver on an AVR processor.
 *  \details This initializes functions to be used in the motor driver class. Function on
 *  turns on the motor. Function off turns off the motor. Function brake toggles the brake
 *  on and off. Function set_power sets how fast the motor will spin and in which direction.
 *  Fuction get_port_values prints the values of the ports. Function check_on returns 0 if the
 *  motor is off, 1 if on. Function check_brake returns 0 if the brake is off, 1 if on.
 *  Function about prints information about the motor such as on/off and power.
 */

class StepperMotor : public Motor
{
    protected:
        /// The instance of StepperMotor being called: 1 or 2.
        uint8_t motor_number;

        /// Contains a pointer to the Stepper Driver associated
        DRV8834_driver* p_stepper;

        /// Contains the current position in steps.
        int32_t position;
        
        /// Contains a pointer to the calibration sensor (IR)
        IRSensor* p_calSensor;


    public:
        StepperMotor (const char*, uint8_t, emstream*);
        virtual ~StepperMotor (void) {};

        // This function turns on the motor.
        virtual void on (void);

        // This function turns off the motor.
        virtual void off (void);

        // This function toggles the brake on and off.
        virtual void brake (void);

        // This sets the position to move to
        virtual void goToPos (int32_t);

        // This sets the velocity to move at.
        virtual void goToVel (int16_t);

        // This returns the position of the stepper
        virtual int32_t getPos (void);

         // This sets the position of the motor
        virtual void setPos (int32_t);

        // This sets the velocity to move at.
        virtual void setVel (int16_t);

        // This calibrates the motor.
        virtual bool calibrate (void);

	// Overloaded << operator for printing functionality.
        friend emstream& operator<< (emstream&, const StepperMotor&);

}; // end of class StepperMotor
#endif // _AVR_StepperMotor_H_
