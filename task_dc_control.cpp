//**************************************************************************************
/** \file task_dc_control.cpp
 *    This file contains the code for a task class which controls two DC motors. 
 *
 *  Revisions:
 *    \li 12-05-2013 GDG Original file created from task_stepper_control
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

/**  The task diagram for this program is shown below. 
  *  \dot
  *  digraph state_diagram {
  *  node [ shape = circle, fontname = FreeSans, fontsize = 10 ];
  * 	State0		[ label = "State 0\nInitialize" ];
  * 	State1		[ label = "State 1\nCalibration" ];
  * 	State2		[ label = "State 2\nPositioning" ];
  *  edge [ arrowhead = normal, style = solid, fontname = FreeSans, fontsize = 10 ];
  * 	State0	-> State1 [ label = "Always" ];
  * 	State1	-> State2 [ label = "test1 && test2 == true" ];
  * 	State2	-> State1 [ label = "flag_motor1cal || flag_motor2cal == true" ];
  *  }
  *  \enddot
  */

#include "frt_text_queue.h"                 // Header for text queue class
#include "task_dc_control.h"                // Header for this task
#include "shares.h"                         // Shared inter-task communications


//-------------------------------------------------------------------------------------
/** This constructor creates a task which controls the rotational position of two DC motors
 *  using input from an encoder. The main job of this constructor is to call the
 *  constructor of parent class (\c frt_task ); the parent's constructor the work.
 *  @param a_name A character string which will be the name of this task
 *  @param a_priority The priority at which this task will initially run (default: 0)
 *  @param a_stack_size The size of this task's stack in bytes 
 *                      (default: configMINIMAL_STACK_SIZE)
 *  @param p_ser_dev Pointer to a serial device (port, radio, SD card, etc.) which can
 *                   be used by this task to communicate (default: NULL)
 */

task_dc_control::task_dc_control (const char* a_name, 
                                 unsigned portBASE_TYPE a_priority, 
                                 size_t a_stack_size,
                                 emstream* p_ser_dev
                                )
    : frt_task (a_name, a_priority, a_stack_size, p_ser_dev)
{
    // Nothing is done in the body of this constructor. All the work is done in the
    // call to the frt_task constructor on the line just above this one
}


//-------------------------------------------------------------------------------------
/** This method is called once by the RTOS scheduler. Each time around the for (;;)
 *  loop, it reads the jumps to the correct state and performs necessary code.
 */

void task_dc_control::run (void)
{
    // Make a variable which will hold times to use for precise task scheduling
    portTickType previousTicks = xTaskGetTickCount ();

    // Initialize a Motor pointer so that c++ doesn't get PO'd from creating it
    // in state 0
    // Also include other variables to be used in loops. Same reason.
    /// First DC motor to be controlled.
    DCMotor* motor1;
    /// Second DC motor to be controlled.
    DCMotor* motor2;
    /// Designates if DC motor1 has been calibrated.
    bool test1;
    /// Designates if DC motor2 has been calibrated .
    bool test2;
    /// Set desired location DC motor1 should try to achieve.
    int16_t des_pos1;
    /// Set desired location DC motor2 should try to achieve.
    int16_t des_pos2;
    /// Actual recorded position from encoder of DC motor1.
    int16_t act_pos1;
    /// Actual recorded position from encoder of DC motor2.
    int16_t act_pos2;
    /// How many ticks have been calculated for the motor to move to.
    int32_t calc_tick;
    ///  The calculated actual position of the motor multiplied by \c DEG_MULT.
    int32_t calc_degX10;
    
    /// Number of teeth on prism gear.
    const uint16_t PRISM_GEAR = 64;
    /// Number of teeth on pinion gear.
    const uint16_t MOTOR_GEAR = 14;
    /// Step down from the gear box attached to each DC motor.
    const uint16_t GEAR_SHAFT = 50;
    /// Number of ticks for a full revolution of the encoder.
    const uint16_t TICKS_PER_REV = 12;
    /// Total degrees in a full rotation.
    const uint16_t DEG_PER_REV = 360;
    /// Scale factor for act_pos calculations.
    const uint8_t  DEG_MULT = 10;
    
    
    
    for (;;)
    {
        // Run the finite state machine. The variable 'state' is kept by the parent class
        switch (state)
        {
            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            // In state 0, we initialize Motor objects and initialize them to zero position.
            // Immediately transition to state 1. (do not return to state 0)
            case (0):
		// Instantiates Motor 1 and turns it on.
                motor1 = new DCMotor("DC1",1,p_serial);
                motor1->on();
		// Instantiates Motor 2 and turns it on.
                motor2 = new DCMotor("DC2",2,p_serial);
                motor2->on();
		// Transition to state 1.
                transition_to (1);
                break;
                
            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            // In state 1, calibrate the motors and transition to 2 when done.
            case (1):
		 // Calibrates motor1 and returns true if completed.
		 test1 = motor1->calibrate();
                // Calibrates motor2 and returns true if completed.
		 test2 = motor2->calibrate();
		 // Proceeds only if both motor1 and motor2 are calibrated.
                if(test1 && test2)
                {
                    // Print that calibrations are comlpete.
                    DBG (p_serial, "Motors Calibrated" << endl);
		    // Set flags that both motor1 and motor2 are calibrated.
                    flag_motor1cal->put(0);
                    flag_motor2cal->put(0);
                    
                    // Initialize the motor PID's.
                    motor1->initPosPID();
                    motor2->initPosPID();
                    
                    // Indicate that calibration has been finished.
                    *print_ser_queue << '<' << ASK_CALIBRATE << '>';
                    
                    // Finally, transition to state 2.
                    transition_to (2);
                }
                break;
                
            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            // In state 2, continuously move towards the desired position.  
            case (2):
		// If either motor flags shows calibration is required.
                if(flag_motor1cal->get() || flag_motor2cal->get())
                {
                    // Transition to state 1 to recalibrate.
                    transition_to (1);
                }
                else
                {
                    // Gets desired position of motor1 from share.
                    des_pos1 = p_share_prism1PosDesX10->get();
		    // Calculates the ticks needed to be read by the encoder to reach desired position.
                    calc_tick = des_pos1;
                    calc_tick = calc_tick*PRISM_GEAR*GEAR_SHAFT*TICKS_PER_REV;
                    calc_tick = calc_tick/DEG_MULT/MOTOR_GEAR/DEG_PER_REV;
		    // Gives motor1 position to move to
                    motor1->goToPos(calc_tick);
                    
		    // Gets desired position of motor2 from share.
                    des_pos2 = p_share_prism2PosDesX10->get();
		    // Calculates the ticks needed to be read by the encoder to reach desired position.
                    calc_tick = des_pos2;
                    calc_tick = calc_tick*PRISM_GEAR*GEAR_SHAFT*TICKS_PER_REV;
                    calc_tick = calc_tick/DEG_MULT/MOTOR_GEAR/DEG_PER_REV;
		    // Gives motor2 position to move to
                    motor2->goToPos(calc_tick);
                    
		    // Get actual position from motor1.
                    calc_degX10 = motor1->getPos();
		    // Calculate the actual position moved to through gear train.
                    calc_degX10 = calc_degX10*DEG_MULT*MOTOR_GEAR*DEG_PER_REV;
                    calc_degX10 = calc_degX10/PRISM_GEAR/TICKS_PER_REV/GEAR_SHAFT;
		    // Saves actual positon.
                    act_pos1 = calc_degX10;
		    // Put actual position on the share.
                    p_share_prism1PosActX10->put(act_pos1);
                    
		    // Get actual position from motor2.
                    calc_degX10 = motor2->getPos();
		    // Calculate the actual position moved to through gear train.
                    calc_degX10 = calc_degX10*DEG_MULT*MOTOR_GEAR*DEG_PER_REV;
                    calc_degX10 = calc_degX10/PRISM_GEAR/TICKS_PER_REV/GEAR_SHAFT;
		    // Saves actual positon.
                    act_pos2 = calc_degX10;
		    // Put actual position on the share.
                    p_share_prism2PosActX10->put(act_pos2);

                }
                break;
                
            default:
                break;
        }

        // Increment the run counter. This counter belongs to the parent class and can
        // be printed out for debugging purposes
        runs++;

        // This is a method we use to cause a task to make one run through its task
        // loop every N milliseconds and let other tasks run at other times
        delay_from_to (previousTicks, configMS_TO_TICKS (10));
    }
}

