//======================================================================================
/** \file IRSensor.h
 *    This file contains the ITSensor class. It contains code that uses an IR receiver
 *    and transmitter as a calibration sensor for the ADC prism location. It is a child
 *    of the CalibrationSensor class.
 *
 *  Revisions:
 *    \li 11-07-2013 CTC Original file
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

#ifndef _AVR_IRSensor_H_
#define _AVR_IRSensor_H_

#include <stdlib.h>                         // Prototype declarations for I/O functions
#include <string.h>                         // Functions for C string handling
#include "emstream.h"                       // Header for serial ports and devices
#include "FreeRTOS.h"                       // Header for the FreeRTOS RTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // Header for FreeRTOS queues
#include "semphr.h"                         // Header for FreeRTOS semaphores
#include "CalibrationSensor.h"              // Header for the CalibrationSensor

//-------------------------------------------------------------------------------------
/** \brief This class creates an IRSensor object that is used as a calibration sensor.
 *  \details This class creates an IRSensor object that is used as a calibration sensor.
 *           It is a child of CalibrationSensor and implements the methods necessary
 *           to calibrate the ADC prisms' positions.
 */
class IRSensor : public CalibrationSensor
{
	protected:
        /// The sensor number. Used as an identifier.
		uint8_t sensorNumber;

    public:
        // The constructor needs the sensor number in addition
        // to parameters for the parent class.
		IRSensor (const char*, uint8_t, emstream*);
		virtual ~IRSensor (void) {};
        // This function initializes any ___
		virtual void startCalibration (void);
		// This function ends any ___
		virtual void endCalibration (void);
		// Returns true when the sensor has been triggered.
        virtual bool isTriggered (void);
        // This method allows you to print an IRSensor object.
        friend emstream& operator<< (emstream&, const IRSensor&);
}; // end of class IRSensor
#endif // _AVR_IRSensor_H_