//**************************************************************************************
/** \file task_user.h
 *    This file contains source code for a user interface task for the atmospheric
 *    dispersion corrector board.
 *
 *  Revisions:
 *    \li 12-02-2013 GDG Original file taken from ME405 code. Adapted to test basic
 *                       functions of StepperMotor.
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

// This define prevents this .h file from being included multiple times in a .cpp file
#ifndef _TASK_USER_H_
#define _TASK_USER_H_

#include <stdlib.h>                         // Prototype declarations for I/O functions

#include "FreeRTOS.h"                       // Primary header for FreeRTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // FreeRTOS inter-task communication queues

#include "rs232int.h"                       // ME405/507 library for serial comm.
#include "time_stamp.h"                     // Class to implement a microsecond timer
#include "frt_task.h"                       // Header for ME405/507 base task class
#include "frt_queue.h"                      // Header of wrapper for FreeRTOS queues
#include "frt_text_queue.h"                 // Header for a "<<" queue class
#include "frt_shared_data.h"                // Header for thread-safe shared data

#include "shares.h"                         // Global ('extern') queue declarations

// Defines used to parse incoming comands from the PC
/// Define Start of Command for parse
#define SOC '<'
/// Define End of Command for parse
#define EOC '>'
/// Define nullchar for end of string
#define NULLCHAR '\0'
/// Define maximum size of incoming or outgoing commands
#define MAX_COMMAND_SIZE 50


//-------------------------------------------------------------------------------------
/** Again, I don't want to delete the following, beacuse it is priceless... But the basic
 *  idea is to allow the user to input pre-determined instructions to the board.  In this
 *  case, instructions via a motor submenu created to control motor 2.
 * 
 *  This task interacts with the user for force him/her to do what he/she is told. What
 *  a rude task this is. Then again, computers tend to be that way; if they're polite
 *  with you, they're probably spying on you. 
 */
class task_user : public frt_task
{
private:
    /// The max size of a command from the PC.
    char inData[MAX_COMMAND_SIZE + 1]; // +1 for the NULL character that terminates char strings.
    /// The index of the command array to keep track of the size.
    uint8_t index;
    /// Indicated whether a command string has started or not.
    bool started;
    /// Allows the program to account for a theta offset, in degrees*10.
    int thetaOffsetX10;

protected:
    // Sends a command string to the PC.
    void send (Commands);
    // Sends a command string and data to the PC.
    void send (Commands, char[]);
    // Parse the command string and act accordingly.
    void parseInput (char[]);
    // Receives a single character from the serial port.
    void receive (char);
    
public:
    // This constructor creates a user interface task object
    task_user (const char*, unsigned portBASE_TYPE, size_t, emstream*);
    // This method loops forever as long as the task is active.
    void run (void);
};

#endif // _TASK_USER_H_
