//======================================================================================
/** \file CalibrationSensor.h
 *    This file contains the CalibrationSensor class. It is a child of the Sensor class
 *    and is the parent of every class used to calibrate the prism position for the
 *    ADC.
 *
 *  Revisions:
 *    \li 11-07-2013 CTC Original file
 *
 *  License:
 *    This file is copyright 2013 by The ADC Team and released under the Lesser GNU
 *    Public License, version 2. */
/*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUEN-
 *    TIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */
//*************************************************************************************

#ifndef _AVR_CalibrationSensor_H_
#define _AVR_CalibrationSensor_H_

#include <stdlib.h>                         // Prototype declarations for I/O functions
#include <string.h>                         // Functions for C string handling
#include "emstream.h"                       // Header for serial ports and devices
#include "FreeRTOS.h"                       // Header for the FreeRTOS RTOS
#include "task.h"                           // Header for FreeRTOS task functions
#include "queue.h"                          // Header for FreeRTOS queues
#include "semphr.h"                         // Header for FreeRTOS semaphores
#include "Sensor.h"                         // Header for Sensor

//-------------------------------------------------------------------------------------
/** \brief This abstract class contains the functions any calibration sensor needs to implement.
 *  \details This abstract class holds the functions any calibration sensor needs to implement.
 */
class CalibrationSensor : public Sensor
{
    protected:
        
    public:
        /// The contructor only takes the variables needed for the parent class.
        CalibrationSensor (const char* newName, emstream* new_ptr_to_serial) :
            Sensor (newName, new_ptr_to_serial)
        {
            // Do nothing
        }
        virtual ~CalibrationSensor (void) {};
        
        /// This function tells the calibration sensor that it is time to calibrate.
        virtual void startCalibration (void) = 0;

        /** This function returns true is the calibration sensor is triggered and
         * false otherwise. It can be polled so you know when to stop the motors.
         */
        virtual bool isTriggered (void) = 0;

}; // end of class CalibrationSensor
#endif // _AVR_CalibrationSensor_H_